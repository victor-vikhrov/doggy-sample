package com.example.victor.surfaceview;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.view.SurfaceHolder;

import java.util.ArrayList;

/**
 * Created by victor on 20.01.18.
 */

public class DrawThread extends Thread {

    private SurfaceHolder surfaceHolder;
    private Bitmap bitmap;

    private volatile boolean isRunning;

    private ArrayList<Dog> dogs = new ArrayList<>();

    public DrawThread(SurfaceHolder holder, Resources resources) {
        this.surfaceHolder = holder;
        this.bitmap = BitmapFactory.decodeResource(resources, R.drawable.dogge);
    }

    public void setRunning(boolean running) {
        this.isRunning = running;
    }

    @Override
    public void run() {
        Canvas canvas;
        while (isRunning) {
            canvas = null;
            try {
                Thread.sleep(40);
                for (Dog d : dogs) {
                    d.size += 1;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            try {
                // получаем объект Canvas и выполняем отрисовку
                canvas = surfaceHolder.lockCanvas(null);
                synchronized (surfaceHolder) { // синхронизация surfaceHolder (только этот имеет доступ к объекту в этом блоке)
                    if (canvas != null) {
                        canvas.drawColor(Color.BLACK);
                        for (int i = 0; i < dogs.size(); i++) {
                            Dog d = dogs.get(i);
                            canvas.drawBitmap(bitmap, new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()),
                                    new Rect(d.x - d.size, d.y - d.size, d.x + d.size, d.y + d.size), null);
                        }
                    }
                }
            } finally {
                if (canvas != null) {
                    // отрисовка выполнена. выводим результат на экран
                    surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
        }
    }

    public void setTouchCoords(int x, int y) {
        dogs.add(new Dog(x, y));
    }

    class Dog {
        int size;
        int x;
        int y;

        Dog(int x, int y) {
            this.x = x;
            this.y = y;
        }


    }

}
